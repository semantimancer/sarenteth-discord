# Sarenteth-Discord

A discord bot for running games of [Sarenteth RPG](http://www.bkugler.com/page.php?page=sarenteth).

This code is still under active development. At the moment it supports a handful of commands which provide information from local files (!slideshow, !wyrd, !contest) as well as a series of campaign commands used for storing campaign-specific information like the present date.

I plan on expanding things as more use-cases come up.
