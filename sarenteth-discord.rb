require 'discordrb'

require_relative 'calendar'

=begin
  USER VARIABLES, VALUES CAN BE CHANGED
=end
$slideshowMax = 18                # Maximum number of !slideshow messages
$slideshowSleep = 10              # Minutes between !slideshow messages
$slideshowFile = "slideshow.txt"  # File to read for !slideshow messages
$contestFile = "contest.txt"      # File to read for !contest messages
$wyrdFile = "wyrd.txt"            # File to read for !wyrd messages
$momentumFile = "momentum.txt"    # File to read for !momentum messages
$spellsFile = "spells.txt"        # File to read for !spells messages
$campaignsDir = "campaigns/"      # File to read for tracking campaign variables

=begin
  INTERNAL VARIABLES, DO NOT CHANGE
=end

CHECKMARK = "\u2714"
$lock = false
$stop = false
$files = [$slideshowFile,$contestFile,$wyrdFile,$momentumFile,$spellsFile]

=begin
  CAMPAIGN-RELATED METHODS & LOGIC
=end

class Campaign
  attr_accessor :cal

  def initialize(name)
    @name = name
    @cal = Calendar.new(1,1,1,3)
    @destinies = Array.new
  end

  def saveCampaign
    File.open("#{$campaignsDir}#{@name}.data", "w") do |f|
      Marshal.dump(self,f)
      f.close
    end
  end

  def displayDestinies
    if @destinies.length > 0
      @destinies.map.with_index { |d,i| "```[#{i}] #{d}```" }.join("\n")
    else
      return "No destinies to display."
    end
  end

  def addDestiny newDest
    @destinies.push(newDest)
  end

  def removeDestiny index
    @destinies.delete_at index if index<@destinies.length
  end
end

# :: String -> Campaign or Nil
# Ruby's still not totally familiar to me. Is there a way to make this a class/instance method?
def loadCampaign name 
  return nil if not File.file? "#{$campaignsDir}#{name}.data"

  File.open("#{$campaignsDir}#{name}.data") do |f|
    marshal = Marshal.load(f)
    f.close
    return marshal
  end
end

def saveChannelCampaigns
  File.open("channelCampaigns.data", "w") do |f|
    Marshal.dump($channelCampaigns,f)
    f.close
  end
end

# Find a campaign and run the specified function over it
def runCampaignCommand event, args, lambdaFunc
  name = args.shift if args

  campaign = loadCampaign name if name
  if not campaign
    args.unshift name if args
    campaign = loadCampaign $channelCampaigns[event.channel.name]
  end

  if campaign
    lambdaFunc.call campaign, args
  else
    "`No valid campaign found.`"
  end
end

=begin
  START OF RUNTIME LOGIC
=end

puts "Initializing..."

# loadCampaign in data for !slideshow, !contest, and !wyrd
messages = $files.map { |f| File.readlines(f).map { |x| x.gsub("\\n","\n") } }

if File.file? "channelCampaigns.data"
  File.open("channelCampaigns.data") do |f|
    $channelCampaigns = Marshal.load(f)
    f.close
  end
else
  $channelCampaigns = Hash.new
end


if (ENV["TOKEN"])
  bot = Discordrb::Commands::CommandBot.new token: ENV["TOKEN"], prefix: "sarenteth!"

  bot.command(:contest, description: "Display overview of contest mechanics.", usage: "contest") do |event|
    event.message.react CHECKMARK
    messages[1].join
  end

  bot.command(:msg, description: "Get X random messages (default 1).", usage: "msg [num]", 
                    min_args: 0, max_args: 1) do |event, num|
    event.message.react CHECKMARK
    if num
      arr=*(1..[num.to_i,1].max).map { |x| messages[0].sample }
      arr.join
    else
      messages[0].sample
    end
  end

  bot.command(:slideshow, description: "Start periodic message service.", usage: "slideshow") do |event|
    event.message.react CHECKMARK
    if $lock
      "Slideshow locked, try again later."
    else
      $lock = true
      event.respond "Slideshow starting (#{$slideshowMax} messages in #{$slideshowSleep}m intervals)"

      # I don't like having this loop 'cause Haskell has me trained, but I'm also too lazy to refactor.
      # Since it kind of has to deal with state and side-effects anyway, it's not worth it.
      1.upto($slideshowMax) do |n|
        if $stop
          puts "Terminating slideshow loop."
          $stop = false
          break
        else
          event.respond messages[0].sample
          sleep (60*$slideshowSleep)
        end
      end
      $lock = false
      return nil # Keep any other return values from being sent after processing's done
    end
  end

  bot.command(:stop, description: "Stop message service early.", usage: "stop") do |event|
    event.message.react CHECKMARK
    if $lock
      $stop = true
      "Stopping slideshow"
    else
      "No slideshow to stop."
    end
  end

  bot.command(:wyrd, description: "Display list of wyrd uses.", usage: "wyrd") do |event|
    event.message.react CHECKMARK
    messages[2].join
  end

  bot.command(:momentum, descriotion: "Display overview of momentum mechanics.", usage: "momentum") do |event|
    event.message.react CHECKMARK
    messages[3].join
  end

  bot.command(:spells, description: "Display overview of spell mechanics.", usage: "spells") do |event|
    event.message.react CHECKMARK
    messages[4].join
  end

  bot.command(:dain, description: "Attempt to defeat Dain.", usage: "dain") do |event|
    event.message.react CHECKMARK
    "Attempting to defeat Dain... Failed. He is still too powerful."
  end

  bot.command(:new_campaign, description: "Create a new campaign.", usage: "new_campaign [name]",
              min_args: 1, max_args: 1) do |event, name|
    event.message.react CHECKMARK

    name.gsub!(/[^a-zA-Z0-9_-]/,'')
    if name
      Campaign.new(name).saveCampaign
      "New campaign #{name} created."
    else
      "`Invalid campaign name.`"
    end
  end

  bot.command(:date, description: "Get current date of specified campaign.",
              usage: "date [campaign]", min_args: 0, max_args: 1) do |event, name|
    event.message.react CHECKMARK

    runCampaignCommand event, [name], (lambda { |campaign, args| campaign.cal.displayDate })
  end

  bot.command(:add_holiday, description: "Add a holiday to the specified campaign's calendar.",
              usage: "add_holiday [campaign] [holiday_name_with_underscores] [month] [day]",
              min_args: 3, max_args: 4) do |event, *args|
    event.message.react CHECKMARK

    runCampaignCommand event, args, (lambda do |campaign, args| 
      name = args.shift.gsub("_"," ")
      month = args.shift
      day = args.shift

      campaign.cal.addHoliday name, month.to_i, day.to_i
      campaign.saveCampaign
      "Holiday #{name} added"
    end)
  end

  bot.command(:remove_holiday, description: "Remove a holiday created via add_holiday.",
              usage: "remove_holiday [campaign] [month] [day]",
              min_args: 2, max_args: 3) do |event, *args|
    event.message.react CHECKMARK

    runCampaignCommand event, args, (lambda do |campaign, args|
      month = args.shift
      day = args.shift

      campaign.cal.removeHolidays month.to_i, day.to_i
      campaign.saveCampaign
      "Holiday on #{month.to_i}/#{day.to_i} removed (if any)"
    end)
  end

  bot.command(:upcoming, description: "Get specified campaign's upcoming holidays.",
              usage: "upcoming [campaign] [days]", min_args: 0, max_args: 2) do |event, *args|
    event.message.react CHECKMARK

    runCampaignCommand event, args, (lambda do |campaign, args|
      num = args.shift
      if num
        campaign.cal.upcoming num.to_i
      else
        campaign.cal.upcoming 30
      end
    end)
  end

  bot.command(:real_date, description: "Get the equivalent Julian calendar date for the specified campaign",
              usage: "real_date [campaign]", min_args: 0, max_args: 1) do |event, name|
    event.message.react CHECKMARK

    runCampaignCommand event, [name], (lambda { |campaign, args| campaign.cal.toReal })
  end

  bot.command(:set_date, description: "Set current date of specified campaign.",
              usage: "set_date [campaign] [month] [day] [year]",
              min_args: 3, max_args: 4) do |event, *args|
    event.message.react CHECKMARK

    runCampaignCommand event, args, (lambda do |campaign, args|
      month = args.shift
      day = args.shift
      year = args.shift

      return "Bad arguments." if (month==nil)||(day==nil)||(year==nil)
      return "Invalid month." if (month.to_i>13)||(month.to_i<0)

      campaign.cal.setDate month.to_i, day.to_i, year.to_i
      campaign.saveCampaign
      "Campaign date is now #{campaign.cal.displayDate}"
    end)
  end

  bot.command(:set_age, description: "Set current age of specified campaign. Defaults to Third.",
              usage: "set_age [campaign] [1-7]",
              min_args: 1, max_args: 2) do |event, *args|
    event.message.react CHECKMARK

    runCampaignCommand event, args, (lambda do |campaign, args|
      campaign.cal.setAge(args.shift.to_i)
      campaign.saveCampaign
      "Campaign date is now #{campaign.cal.displayDate}"
    end)
  end

  bot.command(:moon, description: "Get the phase of the moon in the specified campaign.",
              usage: "moon [campaign]", min_args: 0, max_args: 1) do |event, name|
    event.message.react CHECKMARK

    runCampaignCommand event, [name], (lambda { |campaign, a| campaign.cal.displayMoon })
  end

  bot.command(:add_days, description: "Advance current date of the specified campaign.",
              usage: "add_days [campaign] [days]", min_args: 1, max_args: 2) do |event, *args|
    event.message.react CHECKMARK

    runCampaignCommand event, args, (lambda do |campaign, args|
      campaign.cal.advanceDate args.shift.to_i
      campaign.saveCampaign
      "Campaign date is now #{campaign.cal.displayDate}"
    end)
  end

  bot.command(:destinies, description: "Display active destinies in the specified campaign.",
              usage: "destinies [campaign]", min_args: 0, max_args: 1) do |event, name|
    event.message.react CHECKMARK

    runCampaignCommand event, [name], (lambda { |campaign, a| campaign.displayDestinies })
  end

  bot.command(:add_destiny, description: "Add a new destiny in the specified campaign.",
              usage: "add_destiny [campaign] [destiny]", min_args: 1) do |event, *args|
    event.message.react CHECKMARK

    runCampaignCommand event, args, (lambda do |campaign, args|
      newDestiny = args.join(" ")
      campaign.addDestiny newDestiny
      campaign.saveCampaign
      "New destiny added: ```#{newDestiny}```"
    end)
  end

  bot.command(:remove_destiny, description: "Remove a destiny in the specified campaign.",
              usage: "remove_destiny [campaign] [index]", min_args: 1, ) do |event, *args|
    event.message.react CHECKMARK

    runCampaignCommand event, args, (lambda do |campaign, args|
      index = args.shift
      campaign.removeDestiny index.to_i
      campaign.saveCampaign
      "Destiny #{index} removed."
    end)
  end

  bot.command(:set_channel_campaign, description: "Set default campaign for the channel.",
              usage: "set_channel_campaign [campaign]", min_args: 1, max_args: 1) do |event, name|
    event.message.react CHECKMARK

    runCampaignCommand event, [name], (lambda do |campaign, args|
      $channelCampaigns[event.channel.name] = name
      saveChannelCampaigns
      "Default campaign for \##{event.channel.name} set to #{name}."
    end)
  end

  bot.message(start_with: /(thanks|thank you)/i) do |e|
    if e.content.downcase.end_with?("sarenteth") || e.message.mentions.any? { |u| u.current_bot? }
      e.respond "You're welcome."
    end
  end

  bot.command(:homunculus, description: "Call homunculus with CLI arguments.",
              usage: "homunculus [args]", min_args: 1) do |event, *args|
    # Regexp filtering & length checks are paranoid safeguards to prevent...
    #   * Gaining sh access via ; in the args list and other exploits
    #   * Running homunculus without args, which starts the GUI
    #   * Buffer overflow (don't know if this would be a problem, don't wanna find out)
    event.message.react CHECKMARK
    cArgs = args.join(" ").match(/[0-9a-zA-Z_ !?\.\-]+$/).to_s #.to_s so length works as string
    if cArgs.length<2 || cArgs.length>200
      event.respond "Can't pass bad arguments to the homunculus."
    else
      cmd = `homunculus #{cArgs}`
      if $?.exitstatus > 0
        event.respond "The homunculus isn't behaving. Contact its owner."
      else
        event.respond "The homunculus says:```#{cmd}```"
      end
    end
  end

  bot.run
else
  puts "No TOKEN variable!"
end
