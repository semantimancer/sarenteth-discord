$ages = ["Beginning Years","First Age","Second Age","Third Age","Fourth Age","Fifth Age",
         "Sixth Age","Seventh Age"]
$years = ["Eleventh and last","First","Second","Third","Fourth","Fifth","Sixth","Seventh",
          "Eighth","Ninth","Tenth"]
$months = ["Faeral","Senael","Melael","Lunel","Dorael","Verel","Lecail","Mikiel","Jemael",
           "Narol","Mirel","Trel","Folmael"]
$gods = ["Senill","Mele","Lugeln","Dorn","Vaer","Lecna","Meliki","Jeim","Naroth","Myrr",
         "The Trine","Folimen"]
$seasons = ["Ploughing, Fertilizing","Sowing, Weeding, Ploughing","Pruning, Weeding, Tending",
            "Weeding & Tending", "Shearing & Harvesting", "Ploughing & Gathering",
            "Harvesting, Tying, Winnowing", "Harvesting, Tying, Winnowing, Milling",
            "Sowing, Milling, Weaving", "Butchering, Salting, Smoking, Weaving",
            "Collecting, Digging, Weaving", "Repairing, Planting, Weaving",
            "Ploughing, Fertilizing"]
$specialDays = ["Beltane","Lughnasadh","Samhain","Imbolc"]

class CustomHoliday
  attr_accessor :name
  attr_accessor :month
  attr_accessor :day

  def initialize(name, month, day)
    @month = month
    @day = day
    @name = name
  end
end

class Calendar
  def initialize(month, day, year, age)
    @month = month
    @day = day
    @year = year

    age = 3 if (age>7)||(age<0)
    @age = $ages[age]

    @customHolidays = Array.new()
  end

  def setDate month, day, year
    @month = month%13 if month.is_a? Integer
    @day = day if day.is_a? Integer
    @year = year if year.is_a? Integer
  end

  def setAge age
    age = 3 if (age>7)||(age<0)
    @age = $ages[age]
  end

  def addHoliday hname, hmonth, hday
      if @customHolidays == nil # Backwards Compatibilty
        @customHolidays = Array.new()
      end

      @customHolidays.push(CustomHoliday.new(hname,hmonth,hday))
  end

  def removeHolidays hmonth, hday
    # Deletes just one at a time for now. This is janky and needs to change when I'm not lazy.
    toDelete = @customHolidays.index { |h| (h.month == hmonth)&&(h.day == hday) }
    @customHolidays.delete_at(toDelete)
  end

  def advanceDate num
    @month, @day, @year = getAdvancedDate @month, @day, @year, num
  end

  def displayDate
    retStr = displayShortDate
    retStr << " of the #{@age}."
    retStr << "\n  - Time for #{$seasons[@month]}"
    retStr << "\n  - #{$years[@year%11]} year of the cycle"
    retStr << "\n  - Moon Phase: #{moonPhase}"
    if holidays.length > 0
      holidays.each do |h|
        retStr << "\n  - #{h}." if h
      end
    end

    return retStr
  end

  def displayShortDate month = @month, day = @day, year = @year
    "#{$months[month]} #{day}, Year #{year}"
  end

  def displayMoon
    "The moon is currently #{moonPhase.downcase}."
  end

  def moonPhase
    case @day
    when 1
      ":full_moon:"
    when 2..6
      ":waning_gibbous_moon:"
    when 7
      ":first_quarter_moon:"
    when 8..14
      ":waning_crescent_moon:"
    when 15
      ":new_moon:"
    when 16..21
      ":waxing_crescent_moon:"
    when 22
      ":last_quarter_moon:"
    else
      ":waxing_gibbous_moon:"
    end
  end

  def getEpact y
    epact = 0
    for i in 1..((y-1)%11) do
      epact = (epact+11)%28
    end
    
    return epact
  end

  def holidays month = @month, day = @day, year = @year, customHolidays = @customHolidays
    listing = Array.new()

    if customHolidays
      customHolidays.each do |h|
        if (h.day == day)&&(h.month == month)
          listing.push(h.name)
        end
      end
    end

    case day
    when 1
      if month!=0 #0 is Faeral, which doesn't have a feast day
        if (month%3)==0
          listing.push($specialDays[(month/3)-1])
        else
          listing.push("Feast Day of #{$gods[month-1]}")
        end
      end
    when 2
      if (month%3)==0
        listing.push("Feast Day of #{$gods[month-1]}")
      end
    when 4
      if month==6
        listing.push("Hellnic Day")
      end
      listing.push("Market Day")
    when 15
      listing.push("Fire's Night")
    when 18
      listing.push("Market Day")
    end

    epact = getEpact year

    dayOfYear = day+((month-1)*28)

    if dayOfYear==(44-epact) #Vernal Equinox is on Melael 16 - epact
      listing.push("Ostar (Vernal Equinox)")
    elsif dayOfYear==(134-epact) #Summer Solstice is on Dorael 22 - epact
      listing.push("Midsummer Feast (Summer Solstice)")
    elsif dayOfYear==(252-epact) #Autumnal Equinox is on Mikiel 28 - epact
      listing.push("Feast of Gathering (Autumnal Equinox)")
    elsif dayOfYear==(309-epact) #Yule is on Trel 1 - epact
      listing.push("Yule (Winter Solstice)")
    end

    return listing
  end

  def upcoming num
    puts num
    currMonth = @month
    currDay = @day
    currYear = @year

    upcoming = Array.new
    upcoming.push "**Holidays in the next #{num} days:**"

    num.times do
      currMonth, currDay, currYear = getAdvancedDate currMonth, currDay, currYear, 1
      nextDay = holidays currMonth, currDay, currYear, @customHolidays
      nextDay.each do |i| 
        upcoming.push("  - #{i} on #{displayShortDate currMonth, currDay, currYear}")
      end
    end

    return upcoming.join("\n")
  end

  def toReal
    # Date of Year takes into account a few things
    #   - The day in the campaign calendar
    #   - The month in the campaign calendar (easy because they're standard)
    #   - The epact of the campaign calendar's year
    #   - 38 extra days because the first day of the setting calendar is like mid February
    #   - Extra days (above) cause wrap-around, so %365 keeps us in valid date ranges
    doy = (@day+((@month-1)*28)+(getEpact @year)+38)%365

    retStr = displayShortDate
    retStr << " is roughly equivalent to "
    retStr << Date.strptime(doy.to_s,"%j").strftime("%B %d")

    return retStr
  end
end

def getAdvancedDate month, day, year, num
  cycle = year%11
  if (month==0)&&(cycle==0)&&((day+num)>26) # The Faeral after the 11th year is special
    month = 1
    day = (day+num)-26
  elsif (day+num)>28
    if month==12
      if (cycle==3)||(cycle==6)||(cycle==8)||(cycle==11)
        month = 0 #Faeral
      else
        month = 1 #Senaell
      end
      year = year+1
    else
      month = month+1
    end
    day = (day+num)-28
  else
   day = day+num
  end

  return [month, day, year]
end
