with (import <nixpkgs> {});
let
  gems = bundlerEnv {
    name = "sarenteth-discord";
    inherit ruby;
    gemdir = ./.;
  };
in stdenv.mkDerivation {
  name = "sarenteth-discord";
  buildInputs = [gems ruby];
}
